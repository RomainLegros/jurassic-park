<?php require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest{
    
    public function test_index()
    {   
        $response = $this->make_request("GET", "/");
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
        $this->assertEquals(200, $response->getStatusCode());
    }
}


?>