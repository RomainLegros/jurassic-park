<?php require "vendor/autoload.php";


$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);
Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

Flight::route('/', function(){
    $data = [
        'dinos' => getDinos(),
    ];
    Flight::render('accueil.twig', $data);
});

Flight::route('/dinosaur/@slug', function($slug){
    $data = [
        'dino' => getDino($slug),
        'top' => topRated(),
    ];
    Flight::render('details.twig', $data);
});

Flight::start();


?>